import React from 'react'
import '../assets/styles/main.styl'
import styled from 'styled-components'
import { Provider } from 'mobx-react'
import { getSnapshot } from 'mobx-state-tree'
import App, { Container } from 'next/app'
import { initializeStore } from '../stores/store'

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
`

export default class MyApp extends App {
  // eslint-disable-next-line
  static async getInitialProps({ Component, router, ctx }) {
    //
    // Use getInitialProps as a step in the lifecycle when
    // we can initialize our store
    //
    const isServer = typeof window === 'undefined'
    const store = initializeStore(isServer)
    //
    // Check whether the page being rendered by the App has a
    // static getInitialProps method and if so call it
    //
    let pageProps = {}
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx)
    }
    return {
      initialState: getSnapshot(store),
      isServer,
      pageProps
    }
  }

  constructor(props) {
    super(props)
    this.store = initializeStore(props.isServer, props.initialState)
  }

  render() {
    const { Component, pageProps } = this.props
    return (
      <Container>
        <Provider store={this.store}>
          <Wrapper>
            <Component {...pageProps} />
          </Wrapper>
        </Provider>
      </Container>
    )
  }
}
