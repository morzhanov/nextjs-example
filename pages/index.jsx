import React from 'react'
import { inject, observer } from 'mobx-react'
import Link from 'next/link'
import Header from '../components/Header/Header'
import Container from '../components/Container/Container'

const Home = ({ store }) => (
  <>
    <Header title="Home Page" />
    <Container>
      {store.lastUpdate.toString()}
      {store.light}
      <Link href="/profile">TO PROFILE</Link>
    </Container>
  </>
)

export default inject('store')(observer(Home))
