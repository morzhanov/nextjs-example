import React from 'react'
import { inject, observer } from 'mobx-react'
import Link from 'next/link'
import Header from '../components/Header/Header'
import Container from '../components/Container/Container'

const Profile = ({ store }) => (
  <>
    <Header title="Profile page" />
    <Container>
      {store.lastUpdate.toString()}
      {store.light}
      <Link href="/">TO HOME</Link>
    </Container>
  </>
)

export default inject('store')(observer(Profile))
